import Vue from 'vue'
import Router from 'vue-router'
import BaseView from '@/components/BaseView'
import Dashboard from '@/components/Dashboard'
import Agenda from '@/components/Agenda';
import Login from '@/components/Login';
import Profile from '@/components/Profile'
import Fuel from '@/components/Fuel'
import Mileage from '@/components/Mileage'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/',
            name: 'BaseView',
            component: BaseView,
            children: [{
                path: '/',
                name: 'Dashboard',
                component: Dashboard
            }, {
                path: '/agenda',
                name: 'Agenda',
                component: Agenda
            }, {
                path: '/profile',
                name: 'Profile',
                component: Profile
            }, {
                path: '/fuel',
                name: 'Fuel',
                component: Fuel
            }, {
                path: '/mileage',
                name: 'Mileage',
                component: Mileage
            }]
        }
    ],
    mode: 'history'
})