// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import config from './config';
import axios from 'axios';

Vue.prototype.config = config;
Vue.prototype.axios = axios;

class Global {
    SetBaseView(baseView) {
        this.baseView = baseView;
    }

    GetBaseView() {
        return this.baseView;
    }
}

Vue.prototype.global = new Global();

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})
